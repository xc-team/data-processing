<?php

namespace XcTeam\DataProcessing\DataType;

use SimpleXMLElement;

trait ToXml
{
    public static function jsonToXml($json)
    {
        return self::arrayToXml(self::jsonToArray($json));
    }

    public static function arrayToXml(array $array, $rootElement = 'xml', $xmlObj = null)
    {
        if ($xmlObj === null)
            $xmlObj = new SimpleXMLElement('<' . $rootElement . '/>');
        foreach ($array as $key => $value) {
            if (is_array($value))
                self::arrayToXml($value, $key, $xmlObj->addChild($key));
            else
                $xmlObj->addChild($key, $value);
        }
        return $xmlObj->asXML();
    }

    public static function objectToXml($object)
    {
        return self::arrayToXml(self::objectToArray($object));
    }
}
