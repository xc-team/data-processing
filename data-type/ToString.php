<?php

namespace XcTeam\DataProcessing\DataType;

trait ToString
{
    public static function arrayToString(array $array, $delimiter = ',')
    {
        return implode($delimiter, $array);
    }
}
