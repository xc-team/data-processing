<?php

namespace XcTeam\DataProcessing\DataType;

trait ToArray
{
    public static function xmlToArray($xmlStr)
    {
        $xmlObj = self::xmlToObject($xmlStr);
        return self::objectToArray($xmlObj);
    }

    public static function jsonToArray($json)
    {
        return json_decode($json, true);
    }

    public static function stringToArray($string, $delimiter = ',')
    {
        return explode($delimiter, $string);
    }

    public static function objectToArray(object $object)
    {
        $json = self::objectToJson($object);
        return self::jsonToArray($json);
    }
}
