<?php

namespace XcTeam\DataProcessing\DataType;

trait ToJson
{
    public static function xmlToJson($xmlStr)
    {
        $xmlObj = self::xmlToObject($xmlStr);
        return self::objectToJson($xmlObj);
    }

    public static function arrayToJson(array $array, $isFormatting = false)
    {
        if ($isFormatting)
            return json_encode($array, JSON_UNESCAPED_UNICODE);
        return json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public static function objectToJson(object $object, $isFormatting = false)
    {
        if ($isFormatting)
            return json_encode($object, JSON_UNESCAPED_UNICODE);
        return json_encode($object, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }
}
