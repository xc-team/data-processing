<?php

namespace XcTeam\DataProcessing\DataType;

trait ToObject
{
    public static function xmlToObject($xmlStr)
    {
        return simplexml_load_string($xmlStr, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOERROR);
    }

    public static function jsonToObject($json)
    {
        return json_encode($json);
    }

    public static function arrayToObject($array)
    {
        return self::jsonToObject(self::arrayToJson($array));
    }
}
