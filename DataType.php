<?php

namespace XcTeam\DataProcessing;

use XcTeam\DataProcessing\DataType\ToArray;
use XcTeam\DataProcessing\DataType\ToJson;
use XcTeam\DataProcessing\DataType\ToObject;
use XcTeam\DataProcessing\DataType\ToString;
use XcTeam\DataProcessing\DataType\ToXml;

class DataType
{
    use ToArray, ToJson, ToObject, ToString, ToXml;
}
